# This is the modified Copa UDP based Congestion Control algorithm to run in the mobile Devices #
Original copa can be found in : https://github.com/venkatarun95/genericCC
## In the server side: Run the sender ##
1. To set the ip address in the sender (in the server )please change
udp-socket.cc file line 22 change the ip.
2. Same ip should be used while running sender code: ./sender serverip=128.105.144.241 offduration=0 onduration=60000 cctype=markovian delta_conf=do_ss:auto:0.5 traffic_params=deterministic,num_cycles=1
3. Set the port number in sender.cc file line 23 int sourceport=8080. 
4. So at the sender the socket is created to binded to the above ip and port number.

## In the client side: Run the receiver usually the mobile device ##
1. Modify the port number in line 22 in receiver.cc and IP address in line 81 and port and ip should match with that set in server.cc above.

## compiling the receiver.cc file for Android device ##
	1. you need android_tool from google
		a. NDK needs to be installed in the Linux machine to cross compile and once NDK is installed
		b. $NDK_ROOT/build/tools/make-standalone-toolchain.sh --toolchain=aarch64-linux-android21 --arch=arm64 --install-dir=<directory to get tools:aarch64-linux-android21> --platform=android-21 --force
		c. Now use the android_tool built to get g++ and gcc present in android_tool/bin/
	2. Now we have android tool cross compile the receiver which depends on the udp-socket.cc and hence you have to compile both and link it.
	3. ../verus/android_tool/bin/aarch64-linux-android21-clang++ -I./protobufs-default -I./udt -DHAVE_CONFIG_H -std=c++11 -pthread -pedantic -Wall -Wextra -Weffc++ -Werror  -g -O2 -fPIC -c receiver.cc -o receiver.o
	4. ../verus/android_tool/bin/aarch64-linux-android21-clang++ -I./protobufs-default -I./udt -DHAVE_CONFIG_H -std=c++11 -pthread -pedantic -Wall -Wextra -Weffc++ -Werror -g -O2 -fPIC -c udp-socket.cc -o udp-socket.o
	5. ../verus/android_tool/bin/aarch64-linux-android21-clang++ receiver.o udp-socket.o -o receiver  -lm -pthread
	6. NOw that you have receiver binary do file receiver and it should show as arm64 binary ex: receiver: ELF shared object, 64-bit LSB arm64, dynamic (/system/bin/linker64)
	7. Now copy it to /data/local/tmp and run it as ./receiver


